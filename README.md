# SpeedWarningApp
Đồ án xây dựng ứng dụng di động Cảnh báo tốc độ trên nền tảng react native. Nhóm đã sử dụng ngôn ngữ javascript để xây dựng ứng dụng.

## Chức năng
- Cảnh báo tốc độ tối đa trên các đoạn đường
- Ghi nhận và Lưu trữ nhật ký hành trình di chuyển
- Hỗ trợ cảnh báo âm thanh.
- Tra cứu luật giao thông về tốc độ tối đa của các phương tiên tham gia giao thông 
- Kiểm tra vi phạm
- Chế độ ngày đêm

## Yêu cầu thiết bị
- Hệ điều hành: Android

## Thư viện và công nghệ
- Ứng dụng được lập trình bằng ngôn ngữ react native với expo cli
- Thư viện hỗ trợ: 
    - expo
    - redux, redux-native, redux thunk
    - react native element
    - expo-location, geolib
    - react-native-maps
    - react-native-webview
    - expo-speech, expo-av
    - expo-slite, firebase

## Back end và Database
- nhóm sử dụng firebase firestore để lưu trữ dữ liệu
- Quản lý đồ án trên gitlab

## Hướng dẫn cài đặt
- Bước 1. Nhấn vào link https://expo.dev/accounts/tendouhakai/projects/SpeedWarning/builds/8891615f-b17f-49ab-be09-d186866159e0
- Bước 2. Nhấn nút install trên màn hình và mở camera trên điện thoại để quét mã QR
- Bước 3. Mở link và nhấn install để tải file apk
- Bước 4. Mở file APK và tiến hành cài đặt
- Bước 5. Sau khi cài đặt xong có thể mở app lên và sử dụng
## Demo
Xem sản phẩm demo của nhóm tại link: https://youtu.be/d31i1CQcz3s